## Meowbot Languages

Language files for meowbots output messages.

```sh
1) Go into the translations folder
2) Open en.json
3) Copy en.json to your own file with your country code as the name
4) Under Base, change the language value to your languages name and the code value to the languages code found at https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes under the 639-1 column
5) Translate everything on the right of the : except for anything inside of {}'sh
```