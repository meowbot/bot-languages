// This index.js file will load all other .js file in the directory and sub-directories
'use strict';
var fs = require('fs');
var _ = require('lodash');

function exportDirectory(dir) {
  fs.readdirSync(dir + '/translations/', ['**.json']).forEach(function (file) {
    if (file.indexOf('.') >= 0) {
      exports[file.replace('.json', '').replace('.js', '')] = require(dir + '/translations/' + file);
    }
  });
};

// All files in directory and sub-directories with default naming case
var Languages = function () {
  exportDirectory(__dirname);
  var exps = {};
  _.each(exports, function (value, key, list) {
    exps[key] = value;
  });
  return exports;
};
module.exports = Languages();